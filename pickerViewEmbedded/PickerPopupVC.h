//
//  PickerPopupVC.h
//  UcaDO
//
//  Created by Hamid Mehmood on 14/02/17.
//  Copyright © 2017 Hamid Mehmood. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PickerPopupDelegate <NSObject>

@optional

- (void) returnedPickerViewSelection:(NSDictionary *)selection;

@end

@interface PickerPopupVC : UIViewController


@property (nonatomic, weak) id<PickerPopupDelegate>delegate;

- (id) initWithArray:(NSArray *)dataArray andFrame:(CGRect)frame;


@end
