//
//  AuxPickerData.m
//  pickerViewEmbedded
//
//  Created by Jose Catala on 18/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "AuxPickerData.h"

@implementation AuxPickerData

+ (NSArray *)arrayOfDataForPickerView
{
    return @[
                @{@"id":@1,
                  @"value":@(100),
                  @"text":@"Text For id One"
                  },
                @{@"id":@2,
                  @"value":@(200),
                  @"text":@"Text For id Two"
                  },
                @{@"id":@3,
                  @"value":@(300),
                  @"text":@"Text For id Three"
                  },
                @{@"id":@4,
                  @"value":@(400),
                  @"text":@"Text For id Four"
                  },
                @{@"id":@5,
                  @"value":@(500),
                  @"text":@"Text For id Five"
                  },
                @{@"id":@6,
                  @"value":@(600),
                  @"text":@"Text For id Six"
                  }
                ];
}


@end
