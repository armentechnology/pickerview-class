//
//  PickerPopupVC.m
//  UcaDO
//
//  Created by Hamid Mehmood on 14/02/17.
//  Copyright © 2017 Hamid Mehmood. All rights reserved.
//

#import "PickerPopupVC.h"

@interface PickerPopupVC () <UIPickerViewDelegate, UIPickerViewDataSource, PickerPopupDelegate>
{
    int indexType;
}

@property (strong, nonatomic) NSArray *levelPickerViewArray;
@property (weak, nonatomic) IBOutlet UIPickerView *myPickerView;


@end

@implementation PickerPopupVC

- (id) initWithArray:(NSArray *)dataArray andFrame:(CGRect)frame
{
    self = [super initWithNibName:@"PickerPopupVC" bundle:nil];
    
    if (self)
    {
        
        _levelPickerViewArray = [[NSMutableArray alloc]initWithArray:dataArray];
        
        self.view.frame = frame;
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _myPickerView.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark PICKERVIEW DATASOURCE


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return [_levelPickerViewArray count];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

#pragma mark PICKERVIEW DELEGATE


- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return self.view.frame.size.width;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *returnStr = @"";
    
    if (pickerView == _myPickerView)
    {
        returnStr = [[_levelPickerViewArray objectAtIndex:row] objectForKey:@"text"];
    }
    
    return returnStr;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

#pragma mark BUTTON ACTION

- (IBAction)btnDone:(id)sender
{
    double index = [_myPickerView selectedRowInComponent:0];
    
    NSDictionary *dic = @{
                          @"value":@([[[_levelPickerViewArray objectAtIndex:index] objectForKey:@"value"] integerValue]),
                          @"text":[NSString stringWithFormat:@"%@",[[_levelPickerViewArray objectAtIndex:index] objectForKey:@"value"]],
                          };
    
    [self.delegate returnedPickerViewSelection:dic];
}

@end
