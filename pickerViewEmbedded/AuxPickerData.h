//
//  AuxPickerData.h
//  pickerViewEmbedded
//
//  Created by Jose Catala on 18/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuxPickerData : NSObject

+ (NSArray *) arrayOfDataForPickerView;

@end
