//
//  ViewController.m
//  pickerViewEmbedded
//
//  Created by Jose Catala on 18/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "ViewController.h"
#import "AuxPickerData.h"
#import "PickerPopupVC.h"

@interface ViewController () <PickerPopupDelegate>
{
    PickerPopupVC *pickerView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark ACTION BUTTONS

- (IBAction)btnShowPickerView:(id)sender
{
    if (!pickerView)
    {
        pickerView = [[PickerPopupVC alloc]initWithArray:[AuxPickerData arrayOfDataForPickerView] andFrame:self.view.frame];
        pickerView.delegate = self;
        [self.view addSubview:pickerView.view];
    }
}


#pragma PICKERVIEW DELEGATE

- (void) returnedPickerViewSelection:(NSDictionary *)selection
{
    [pickerView.view removeFromSuperview];
    
    pickerView = nil;  
}

@end
